import os
import pathlib

import l18n
import l18n.utils
from cleo.events.console_command_event import ConsoleCommandEvent
from cleo.events.console_events import COMMAND
from cleo.events.event_dispatcher import EventDispatcher
from poetry.console.application import Application
from poetry.console.commands.build import BuildCommand
from poetry.console.commands.install import InstallCommand
from poetry.plugins.application_plugin import ApplicationPlugin

ZONE_BASE_DIR = pathlib.Path(os.environ.get("ZONEINFO_DIR", "/usr/share/zoneinfo"))


def timezones():
    """reads /usr/share/zoneinfo/zone.tab and extracts zone names"""
    with (ZONE_BASE_DIR / "zone.tab").open() as f:
        line = f.readline()
        while line:
            stripped = line.strip()
            line = f.readline()
            if stripped.startswith("#") or not stripped:
                continue  # comments or empty lines
            yield [e for e in stripped.split() if e][2]


def extract_gnu_tz(timezone):
    with (ZONE_BASE_DIR / timezone).open("rb") as f:
        data = f.read()[:-1]  # remove last newline
        last_new_line = data.rindex(b"\n")
        return data[last_new_line + 1 :].decode()


def create_file(path: pathlib.Path):
    with path.open("w") as f:
        f.write("_tzdata = [\n")
        for timezone in timezones():
            country_code = l18n.utils.get_country_code_from_tz(timezone.replace(" ", "_"))
            country_name = l18n.territories.get(country_code)
            city = l18n.tz_cities.get(timezone.replace(" ", "_"), None)
            if not city:
                # latest version of l18n doesn't know the timezone yet
                continue
            gnu = extract_gnu_tz(timezone)
            f.write(f'    ("{timezone}", "{country_code}", "{country_name}", "{city}", "{gnu}"),\n')
        f.write("]\n")
        f.write("\n\n")
        f.write("TZ_GNU = {e[0]: e[4] for e in _tzdata}\n")
        f.write("COUNTRIES = {e[1]: e[2] for e in _tzdata}\n")


class TurrisTimezonePlugin(ApplicationPlugin):
    def activate(self, application: Application):
        if config := application.poetry.pyproject.data.get("tool", {}).get("turris_timezone_poetry_plugin"):
            if config.get("enabled", False) is True:
                if application.event_dispatcher:
                    application.event_dispatcher.add_listener(COMMAND, self.prepare_turris_timezone_file)

        self._stored_application = application

    def prepare_turris_timezone_file(
        self,
        event: ConsoleCommandEvent,
        event_name: str,
        dispatcher: EventDispatcher,
    ):
        command = event.command
        io = event.io

        if not isinstance(command, (BuildCommand, InstallCommand)):
            return

        io.write_line("Creating timezone file")

        # Detect file path
        if root_dir := self._stored_application.poetry.package.root_dir:
            relative_path = self._stored_application.poetry.pyproject.data.get("tool", {})[
                "turris_timezone_poetry_plugin"
            ]["output_path"]

            path = root_dir / str(relative_path)
            create_file(path)

        pass
